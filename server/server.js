import express from 'express';
import webpack from 'webpack';
import path from 'path';
import config from '../config/dev';

var app = express();
var compiler = webpack( config.webpack );

app.use( require( 'webpack-dev-middleware' ) ( compiler, {
	publicPath : config.webpack.output.publicPath
} ) );

app.use( require( 'webpack-hot-middleware' ) ( compiler ) );

app.use( express.static( path.join( __dirname, '/' ) ) );

app.listen( process.env.PORT || 4500, '0.0.0.0', function ( err ) {
	if ( err ) {
		return console.log( err );
	}

	console.log( 'server started!' );
} );
