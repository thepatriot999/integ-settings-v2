## Installation
`npm install`


## Running for local development
`npm run dev`

## Running the linter
`npm run lint`
