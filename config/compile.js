import path from 'path';
import webpack from 'webpack';
import HtmlWebpackPlugin from 'html-webpack-plugin';

module.exports = {
	'webpack' : {
		'resolve' : {
			'root' : [
				path.resolve( 'src' ),
				path.resolve( 'node_modules' )
			],
			'extensions' : [ '', '.js', '.jsx' ]
		},
		'output' : {
			'path'     : 'dist',
			'filename' : '[name].bundle.js',
			'chunkFilename' : '[id].bundle.js'
		},
		'entry' : {
			'init' : './src/init',
			'main' : './src/main',
		},
		'module' : {
			'preLoaders' : [],
			'loaders'    : [
				{
					'test'    : /\.(js|jsx)$/,
					'loaders'  : [ 'babel?presets[]=es2015,presets[]=react' ],
					'exclude' : /node_modules/
				},
				{
					'test'    : /\.(jpe?g|png|gif|svg)$/i,
					'loaders' : [
						'file?hash=sha512&digest=hex&name=[hash].[ext]',
						'image-webpack?bypassOnDebug&optimizationLevel=7&interlaced=false'
					]
				},
				{
					'test'    : /\.css$/,
					'loaders' : [
						'style', 'css', 'postcss'
					]
				},
				{
					'test'    : /\.scss$/,
					'loaders' : [
						'style', 'css', 'sass', 'postcss'
					]
				},
				{
					'test'   : /\.woff(2)?(\?v=[0-9]\.[0-9]\.[0-9])?$/,
					'loader' : 'url-loader?limit=10000&mimetype=application/font-woff'
				},
				{
					'test'   : /\.(ttf|eot|svg)(\?v=[0-9]\.[0-9]\.[0-9])?$/,
					'loader' : 'file-loader'
				}
			]
		},
		'plugins' : [
			new HtmlWebpackPlugin( {
				'template' : 'pages/user.html',
				'filename' : 'user.html',
				'inject'   : 'body',
				'chunks'   : [ 'init', 'user' ]
			} ),
			new webpack.optimize.UglifyJsPlugin(),
			new webpack.optimize.DedupePlugin()
		]
	}
};
