import path from 'path';
import webpack from 'webpack';
import HtmlWebpackPlugin from 'html-webpack-plugin';

module.exports = {
	'webpack' : {
		'resolve' : {
			'root' : [
				path.resolve( 'src' ),
				path.resolve( 'node_modules' )
			],
			'extensions' : [ '', '.js', '.jsx' ]
		},
		'output' : {
			'path'     : path.join( __dirname, 'dist' ),
			'filename' : '[name].bundle.js',
			'publicPath' : '/'
		},
		'entry' : {
			'init' : './src/init.js',
			'main' : [ 'webpack-hot-middleware/client', './src/main.js' ]
		},
		'module' : {
			'preLoaders' : [],
			'loaders'    : [
				{
					'test'    : /\.(js)$/,
					'exclude' : /node_modules/,
					'loaders'  : [ 'react-hot', 'babel?presets[]=es2015,presets[]=react' ]
				},
				{
					'test'    : /\.(jpe?g|png|gif|svg)$/i,
					'loaders' : [
						'file?hash=sha512&digest=hex&name=[hash].[ext]',
						'image-webpack?bypassOnDebug&optimizationLevel=7&interlaced=false'
					]
				},
				{
					'test'    : /\.css$/,
					'loaders' : [
						'style', 'css', 'postcss'
					]
				},
				{
					'test'    : /\.scss$/,
					'loaders' : [
						'style', 'css', 'sass', 'postcss'
					]
				},
				{
					'test'   : /\.woff(2)?(\?v=[0-9]\.[0-9]\.[0-9])?$/,
					'loader' : 'url-loader?limit=10000&mimetype=application/font-woff'
				},
				{
					'test'   : /\.(ttf|eot|svg)(\?v=[0-9]\.[0-9]\.[0-9])?$/,
					'loader' : 'file-loader'
				}
			]
		},
		'plugins' : [
			new HtmlWebpackPlugin( {
				'template' : 'pages/index.html',
				'filename' : 'index.html',
				'inject'   : 'body',
				'chunks' : [ 'init', 'main' ]
			} ),
			new webpack.HotModuleReplacementPlugin()
		]
	}
};
