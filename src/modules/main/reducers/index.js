import {
	ADD_ITEM
} from '../actions';

const initialState = {};

export default function mainReducer ( state = initialState, item ) {
	switch ( item.type ) {
		case ADD_ITEM:
			return Object.assign( {},
				state,
				item.item );
		default:
			return state;
	}
}
