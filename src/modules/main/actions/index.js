export const ADD_ITEM = 'ADD_ITEM';

export function addItem ( item ) {
	return {
		item,
		'action' : ADD_ITEM
	};
}
