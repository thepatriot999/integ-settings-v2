import React from 'react';
import Checkbox from 'material-ui/Checkbox';
import Popover from 'material-ui/Popover';
import RaisedButton from 'material-ui/RaisedButton';

export default class Main extends React.Component {

	componentWillMount () {
		this.setState( {
			'open' : false,
			'anchor' : null
		} );
	}

	showPopOver ( evt ) {
		this.setState( {
			'open' : true,
			'anchor' : evt.currentTarget
		} );
	}

	requestClose ( evt ) {
		this.setState( {
			'open' : false
		} );
	}

	render () {
		let style = {
			minWidth : '120px'
		};

		return (
			<div>
				Sample page
				<RaisedButton style={ style } onClick={ this.showPopOver.bind( this ) } label="Job Status" />
				<Popover
				style={ style }
				onRequestClose={ this.requestClose.bind( this ) }
				anchorEl={ this.state.anchor }
				targetOrigin={
					{
						horizontal: 'left',
						vertical: 'top'
					}
				}
				anchorOrigin={
					{
						horizontal: 'left',
						vertical: 'bottom'
					}
				}
				open={ this.state.open }>
				<Checkbox label="Test" />
				<Checkbox label="Test" />
				<Checkbox label="Test" />
				<Checkbox label="Test" />
				</Popover>
			</div>
		);
	}
}
