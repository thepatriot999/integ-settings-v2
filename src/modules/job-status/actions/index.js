export const FILTER_CHANGED = 'FILTER_CHANGED';

export function filterChanged ( item ) {
	return {
		'payload' : item,
		'type' : FILTER_CHANGED
	};
}
