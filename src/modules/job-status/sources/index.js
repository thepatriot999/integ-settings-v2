import http from '../../../common/http';
import moment from 'moment';

export default {
	searchJobStatus ( jobList ) {
		return new Promise ( ( resolve, reject ) => {
			resolve( [
				{
					'posting_id' : 96577231,
					'job_id' : 1234224,
					'site_id' : 12222,
					'site_name' : 'Monster.com',
					'accountname' : '12v3123vksjdnakjnsdjkasnd',
					'job_status' : 'pending_transmit_waiting',
					'job_status_added' : moment().format( 'Y-MM-DD h:mm:ss' ),
					'packet_id' : 123456,
					'packet_id_added' : moment().format( 'Y-MM-DD h:mm:ss' ),
					'integration_type' : 'incremental',
					'transmitter' : 38,
					'denorm' : 'not_denorm'
				},
				{
					'posting_id' : 96577232,
					'job_id' : 1234224,
					'site_id' : 12222,
					'site_name' : 'Monster.com',
					'accountname' : 'test_accounts',
					'job_status' : 'pending_automated_confirm',
					'job_status_added' : moment().format( 'Y-MM-DD h:mm:ss' ),
					'packet_id' : 123456,
					'packet_id_added' : moment().format( 'Y-MM-DD h:mm:ss' ),
					'integration_type' : 'full_account',
					'transmitter' : 39,
					'denorm' : 'denorm'
				},
				{
					'posting_id' : 96577233,
					'job_id' : 1234224,
					'site_id' : 12222,
					'site_name' : 'Monster.com',
					'accountname' : '',
					'job_status' : 'pending_automated_delete',
					'job_status_added' : moment().format( 'Y-MM-DD h:mm:ss' ),
					'packet_id' : 123456,
					'packet_id_added' : moment().format( 'Y-MM-DD h:mm:ss' ),
					'integration_type' : 'full',
					'transmitter' : 38,
					'denorm' : 'not_denorm'
				},
				{
					'posting_id' : 96577234,
					'job_id' : 1234224,
					'site_id' : 12222,
					'site_name' : 'Monster.com',
					'accountname' : '',
					'job_status' : 'pending_delete',
					'job_status_added' : moment().format( 'Y-MM-DD h:mm:ss' ),
					'packet_id' : 123456,
					'packet_id_added' : moment().format( 'Y-MM-DD h:mm:ss' ),
					'integration_type' : 'full',
					'transmitter' : 38,
					'denorm' : 'not_denorm'
				},
				{
					'posting_id' : 96577235,
					'job_id' : 1234224,
					'site_id' : 12222,
					'site_name' : 'Monster.com',
					'accountname' : '',
					'job_status' : 'pending_automated_confirm',
					'job_status_added' : moment().format( 'Y-MM-DD h:mm:ss' ),
					'packet_id' : 123456,
					'packet_id_added' : moment().format( 'Y-MM-DD h:mm:ss' ),
					'integration_type' : 'full',
					'transmitter' : 22,
					'denorm' : 'no_denorm_data'
				}
			] );
		} );
	}
};
