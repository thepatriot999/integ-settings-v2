import {
	FILTER_CHANGED
} from '../actions';

const initialState = {
	'filters' : {
		'integration_type_filter' : 0,
		'integration_class_filter' : 0,
		'status_filter' : 0
	}
};

export default function jobStatusReducer( state = initialState, item ) {
	switch ( item.type ) {
		case FILTER_CHANGED:
			return Object.assign( {},
				state,
				{
					'filters' : item.payload
				} );
			break;
		default:
			return state;
	}
};
