import { Table,
	TableBody,
	TableFooter,
	TableHeader,
	TableHeaderColumn,
	TableRow,
	TableRowColumn
} from 'material-ui/Table';
import React from 'react';
import moment from 'moment';

export default class JobList extends React.Component {

	componentWillMount () {
		this.setState( {
			'list' : []
		} )
	}

	render () {
		let list = [];
		
		if ( this.props.list ) {
			list = this.props.list;

			if ( typeof list === 'object' ) {
				list = Object.keys( list ).map( ( key ) => {
					return list[ key ];
				} );
			}
		}

		let tableContent = list.map( ( item, index ) => {
			let denorm = <i className="fa fa-check-square" aria-hidden="true"></i>;

			if ( item.denorm === 'not_denorm' ) {
				denorm = <i className="fa fa-circle-o" aria-hidden="true"></i>;
			} else if ( item.denorm === 'no_denorm_data' ) {
				denorm = <i className="fa fa-times" aria-hidden="true"></i>;
			}

			return <TableRow key={ index }>
					<TableRowColumn className="integration-type">
						{ item.integration_type }
					</TableRowColumn>
					<TableRowColumn className="posting-id">
						{ item.posting_id }
					</TableRowColumn>
					<TableRowColumn className="job-id">
						{ item.job_id }
					</TableRowColumn>
					<TableRowColumn className="site-id">
						{ item.site_id }
					</TableRowColumn>
					<TableRowColumn className="accountname">
						{ item.accountname }
					</TableRowColumn>
					<TableRowColumn className="job-site-status">
						{ item.job_status }
					</TableRowColumn>
					<TableRowColumn className="job-status-added">
						{ moment( item.job_status_added ).format( 'Y-m-D h:mm' ) }
					</TableRowColumn>
					<TableRowColumn className="packet-id">
						{ item.packet_id }
					</TableRowColumn>
					<TableRowColumn className="packet-id-added">
						{ moment( item.packet_id_added ).format( 'Y-m-D h:mm' ) }
					</TableRowColumn>
					<TableRowColumn className="denorm">
						{ denorm }
					</TableRowColumn>
				</TableRow>;
		} );

		return (
			<div className="job-list">
				<Table selectable={ false }>
					<TableHeader adjustForCheckbox={ false } displaySelectAll={ false }>
						<TableRow>
							<TableHeaderColumn className="integration-type"> Type </TableHeaderColumn>
							<TableHeaderColumn className="posting-id"> Posting ID </TableHeaderColumn>
							<TableHeaderColumn className="job-id"> Job ID </TableHeaderColumn>
							<TableHeaderColumn className="site-id"> Site </TableHeaderColumn>
							<TableHeaderColumn className="accountname"> Account Name </TableHeaderColumn>
							<TableHeaderColumn className="job-site-status"> Status </TableHeaderColumn>
							<TableHeaderColumn className="job-status-added"> Status Added </TableHeaderColumn>
							<TableHeaderColumn className="packet-id"> Packet ID </TableHeaderColumn>
							<TableHeaderColumn className="packet-id-added"> Packet Added </TableHeaderColumn>
							<TableHeaderColumn className="denorm"> Denorm </TableHeaderColumn>
						</TableRow>
					</TableHeader>
					<TableBody displayRowCheckbox={ false }>
						{ tableContent }
					</TableBody>
				</Table>
			</div>
		);
	}
}
