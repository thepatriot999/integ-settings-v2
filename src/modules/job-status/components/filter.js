import React from 'react';
import {
	Toolbar,
	ToolbarGroup,
	ToolbarSeparator,
	ToolbarTitle
} from 'material-ui/Toolbar';
import DropDownMenu from 'material-ui/DropDownMenu';
import MenuItem from 'material-ui/MenuItem';
import TextField from 'material-ui/TextField';
import Checkbox from 'material-ui/Checkbox';
import RaisedButton from 'material-ui/RaisedButton';
import IconButton from 'material-ui/IconButton';
import {
	FILTER_TYPES,
	applyFilter
} from '../../../common/filter-util';
import {
	filterChanged
} from '../actions';
import store from '../.././../store';
import DropDownButton from '../.././../common/drop-down-button';

const integrationTypesList = [
	'full_account',
	'full',
	'incremental'
];

const statuses = [
	'automated_error',
	'pending_transmit',
	'pending_update',
	'pending_delete',
	'pending_transmit_waiting',
	'pending_confirm',
	'pending_automated_confirm',
	'pending_automated_delete',
	'posted',
	'expired',
	'account_services'
];

export default class Filter extends React.Component {

	componentWillMount () {
		this.setState( {
			'filters' : {
				'integration_type_filter' : 0,
				'integration_class_filter' : 0,
				'status_filter' : 0
			},
			'status_popover_show' : false,
			'status_popover_anchor' : null
		} );
	}

	filterFieldValueChanged ( type ) {
		return ( evt, index, value ) => {
			let field = {};

			field[ type ] = value;

			let filters = Object.assign( {}, this.state.filters, field );

			this.setState( {
				'filters' : filters
			} );

			this.triggerFiltersChange( filters );
		}
	}

	triggerFiltersChange ( filters ) {
		if ( this.props.onFiltersChanged && typeof this.props.onFiltersChanged == 'function' ) {
			this.props.onFiltersChanged( filters );
		}
	}

	statusFilterChanged ( filter ) {
		let filters = Object.assign( {}, this.state.filters );

		filters.status_filter = filter;

		this.setState( {
			'filters' : filters
		} );

		this.triggerFiltersChange( filters );
	}

	integrationTypeFilterChanged ( filter ) {
		let filters = Object.assign( {}, this.state.filters );

		filters.integration_type_filter = filter;

		this.setState( {
			'filters' : filters
		} );

		this.triggerFiltersChange( filters );
	}

	refreshFilteredJobs () {
		if ( this.props.refreshFilteredJobs && typeof this.props.refreshFilteredJobs == 'function' ) {
			this.props.refreshFilteredJobs();
		}
	}

	render () {
		let statusTypes = FILTER_TYPES.status_types.map( ( statusType, index ) => {
			return <MenuItem key={ index } value={ index } primaryText={ statusType } />
		} );

		let integrationClasses = FILTER_TYPES.integration_class.map( ( type, index ) => {
			return <MenuItem key={ index } value={ index } primaryText={ type } />
		} );

		let integrationTypes = FILTER_TYPES.integration_types.map( ( type, index ) => {
			return <MenuItem key={ index } value={ index } primaryText={ type } />
		} );

		let dropDownStyle = {
			'marginRight' : '30px'
		};

		return (
			<Toolbar className="filters">
				<ToolbarGroup>
					<ToolbarTitle text="Filter" />
					<ToolbarSeparator />
					<DropDownMenu className="integration-class-dropdown" autoWidth={ false } onChange={ this.filterFieldValueChanged( 'integration_class_filter' ).bind( this ) } value={ this.state.filters.integration_class_filter }>
						{ integrationClasses }
					</DropDownMenu>
					<DropDownButton style={ dropDownStyle } checkedItemsChanged={ this.statusFilterChanged.bind( this ) } items={ statuses } label="Status Filter" />
					<DropDownButton style={ dropDownStyle } checkedItemsChanged={ this.integrationTypeFilterChanged.bind( this ) } items={ integrationTypesList } label="Integration Types" />
					<ToolbarSeparator />
					<TextField className="search-input" hintText="Search" />
					<RaisedButton label="Filter Input" primary={ true } />
					<IconButton onTouchTap={ this.props.refreshFilteredJobs.bind( this ) } iconClassName="fa fa-refresh" />
				</ToolbarGroup>
			</Toolbar>
		);
	}
}
