import React from 'react';
import RaisedButton from 'material-ui/RaisedButton';
import JobList from './job-list';
import Filter from './filter';
import moment from 'moment';
import {
	applyFilter,
	formatAndCleanRawList,
	normalizePostingList
} from '../../../common/filter-util';
import store from '../.././../store';
import source from '../sources';

export default class JobStatus extends React.Component {

	componentWillMount () {
		this.setState( {
			'posting_list' : [],
			'filters' : false
		} );
	}

	componentWillUnmount () {
		this.unsubscribe();
	}


	/*
	 * We are going to retrieve the statuses of the posting id's provided
	 */
	loadJobStatus () {
		let rawList = formatAndCleanRawList( this.refs.raw_job_list.value );

		source.searchJobStatus( rawList.join( ',' ) )
			.then( ( jobs ) => {
				this.setState( {
					'posting_list' : jobs
				} );
			} )
			.catch( ( error ) => {
				console.log( error );
			} );
	}

	filtersChangedHandler ( filters ) {
		this.setState( {
			'filters' : filters
		} );
	}

	refreshFilteredJobsHandler () {
		let filtered = this.state.posting_list;

		if ( this.state.filters ) {
			filtered = applyFilter( this.state.filters, filtered );			
		}

		filtered = filtered.map( ( posting ) => {
			return posting.posting_id;
		} ).join( ',' );

		source.searchJobStatus( filtered )
			.then( ( jobs ) => {
				let postings = this.state.posting_list;

				jobs = normalizePostingList( jobs );
				postings = normalizePostingList( postings );

				postings = Object.keys( postings ).map( ( posting_id ) => {
					if ( jobs[ posting_id ] ) {
						return jobs[ posting_id ];
					}

					return postings[ posting_id ];
				} );

				this.setState( {
					'posting_list' : postings
				} );
			} )
			.catch( ( error ) => {
				console.log( error );
			} );
	}

	render () {
		let filtered = this.state.posting_list;

		if ( this.state.filters ) {
			filtered = applyFilter( this.state.filters, filtered );			
		}

		return (
			<div className="job-status">
				<div className="left-content">
					<RaisedButton onTouchTap={ this.loadJobStatus.bind( this ) } fullWidth={ true } className="btn-process" label="Reload"/>
					<textarea ref="raw_job_list">
					</textarea>
				</div>
				<div className="right-content">
					<Filter refreshFilteredJobs={ this.refreshFilteredJobsHandler.bind( this ) } onFiltersChanged={ this.filtersChangedHandler.bind( this ) } />
					<JobList list={ filtered } />
				</div>
			</div>
		);
	}
}
