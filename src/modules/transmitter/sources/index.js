import http from '../../../common/http';

export default {
	getTransmitters () {
		return http.get( '/transmitter' );
	},

	save ( data ) {
		return http.post( '/transmitter', data );
	},

	update ( data ) {
		return http.put( '/transmitter', data );
	},

	del ( id ) {
		return http.del( `/transmitter/${ id }` );
	}
}
