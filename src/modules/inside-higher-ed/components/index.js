import React from 'react';
import TearSheet from '../../../common/tear-sheet';
import SubHeader from 'material-ui/SubHeader';
import { List, ListItem } from 'material-ui/List';
import source from '../sources';
import config from '../../../config';
import CategoryList from './category-list';

export default class InsideHigherEd extends React.Component {

	componentWillMount () {
		this.setState( {
			'categories' : [],
			'products'   : []
		} );

		this.getToken();
	}

	getToken () {
		source.authenticate( config.ihe_api.login_id, config.ihe_api.password )
			.then( ( response ) => {
				let token = response.string._;

				source.getProducts( token )
					.then( ( productResponse ) => {
						let products = productResponse.ArrayOfWSProduct.$$.WSProduct;

						this.setState( {
							'products' : this.normalizeProducts( products )
						} );
					} )
					.catch( ( error ) => {
						console.log( error );
					} );

				source.getCategories( token )
					.then( ( categoriesResponse ) => {
						let categories = categoriesResponse.ArrayOfWSCategory.$$.WSCategory;
						let categoryTerms = {};

						let normalizedCategories = this.normalizeCategories( categories );

						Promise.all( normalizedCategories.map( ( category ) => {
							return source.getCategoryTerms( category.ID, token );
						} ) ).then( ( response ) => {
							response.forEach( ( categoryTermsResponse ) => {
								if ( categoryTermsResponse.terms.ArrayOfWSHierarchicalTerm.$$ ) {
									categoryTerms[ categoryTermsResponse.category_id ] = categoryTermsResponse.terms.ArrayOfWSHierarchicalTerm.$$.WSHierarchicalTerm.map( ( term ) => {
										let children = [];

										if ( term.$$.Children && term.$$.Children[ 0 ].$$  ) {
											children = this.normalizeTermChildren( term.$$.Children[ 0 ].$$.WSHierarchicalTerm );
										}

										return {
											'ID' 	   : term.$$.ID[ 0 ],
											'Name' 	   : term.$$.Name[ 0 ],
											'Children' : children
										}
									} );
								}

								this.setState( {
									'categories' : this.normalizeCategoryTerms( categoryTerms, normalizedCategories )
								} );
							} )
						} )
						.catch( ( error ) => {
							console.log( error );
						} );
					} )
					.catch( ( error ) => {
						console.log( error );
					} )
			} )
			.catch( ( error ) => {
				console.log( error );
			} );	
	}

	normalizeProducts ( products ) {
		return products.map( ( product ) => {
			return {
				'Name' : product.$$.Name[0],
				'Id' : product.$$.Id[0],
				'Duration' : product.$$.Duration[0],
				'DurationUnit' : product.$$.DurationUnit[0],
			}
		} );
	}

	normalizeCategories ( categories ) {
		return categories.map( ( category ) => {
			return {
				'Name' : category.$$.Name[ 0 ],
				'Mandatory' : category.$$.Mandatory[ 0 ],
				'MultiSelect' : category.$$.MultiSelect[ 0 ],
				'ID' : category.$$.ID[ 0 ]
			}
		} );
	}

	normalizeCategoryTerms ( categoryTerms, categories ) {
		return categories.map( ( category ) => {
			if ( categoryTerms[ category.ID ] ) {
				return Object.assign( {},
					category,
					{
						'terms' : categoryTerms[ category.ID ]
					}
				);
			}

			return category;
		} );
	}

	normalizeTermChildren ( termChildren ) {
		return termChildren.map( ( termChild ) => {
			return {
				'Name' : termChild.$$.Name[ 0 ],
				'ID'   : termChild.$$.ID[ 0 ]
			}
		} );
	}

	render () {
		let productList = this.state.products.map( ( product ) => {
			let primary = `${ product.Name }`;
			let duration = <span> { `${ product.Duration } ${ product.DurationUnit }` } </span>;
			let id = `ID: ${ product.Id }`;

			return <ListItem key={ product.Id } secondaryText={ id } primaryText={ primary } leftIcon={ duration } />;
		} );

		return (
			<div className="inside-higher-ed">
				<div className="module-title">
					Inside Higher Ed API Access
				</div>
				<div className="display-list">
					<TearSheet className="product-list">
						<List>
							<SubHeader> Product List </SubHeader>
							{ productList }
						</List>
					</TearSheet>
					<CategoryList categories={ this.state.categories } />
				</div>
			</div>
		);
	}
}
