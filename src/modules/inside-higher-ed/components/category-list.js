import React from 'react';
import SubHeader from 'material-ui/SubHeader';
import TearSheet from '../../../common/tear-sheet';
import { List, ListItem } from 'material-ui/List';

export default class CategoryList extends React.Component {

	componentWillMount () {
		this.setState( {
			'categories' : []
		} );
	}

	componentWillReceiveProps ( props ) {
		this.setState( {
			'categories' : props.categories
		} );
	}

	render () {
		let categoryList = this.state.categories.map( ( category ) => {
			let isMandatory = '';
			let isMultiple = '';

			if ( category.MultiSelect === 'true' ) {
				isMultiple = <i className="fa fa-check-circle" aria-hidden="true"></i>;
			}

			if ( category.Mandatory === 'true' ) {
				isMandatory = <i className="fa fa-check-circle" aria-hidden="true"></i>;
			}

			let primary = <div className="primary">
					<div className="cat-name"> { `${ category.ID } - ${ category.Name }` } </div>
					<div className="mandatory">
						{ isMandatory }
					</div>
					<div className="multiple">
						{ isMultiple }
					</div>
				</div>;
			let secondary = `ID: ${ category.ID } `;

			if ( category.terms ) {
				let nestedListItems = category.terms.map( ( term ) => {
					let termChildren = [];

					if ( term.Children ) {
						termChildren = term.Children.map( ( child ) => {
							return <ListItem key={ child.ID } secondaryText={ child.ID } primaryText={ child.Name } />;
						} );
					}

					return <ListItem nestedItems={ termChildren } key={ term.ID } secondaryText={ term.ID } primaryText={ term.Name } />;
				} );

				return <ListItem className="list-item" nestedItems={ nestedListItems } key={ category.ID } primaryText={ primary } />;
			}

			return <ListItem key={ category.ID } primaryText={ primary } />;
		} );

		return (
			<TearSheet className="category-list">
				<List>
					<SubHeader className="list-header">
						<span> Category List </span>
						<span className="required"> Required </span>
						<span className="multiple"> Multiple </span>
					</SubHeader>
					{ categoryList }
				</List>
			</TearSheet>
		);
	}
}
