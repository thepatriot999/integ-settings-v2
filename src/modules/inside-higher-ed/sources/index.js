import superagent from 'superagent';
import xml2js from 'xml2js';
import config from '../../../config';

export default {
	authenticate ( loginId, password ) {
		return new Promise( ( resolve, reject ) => {
			superagent.get( `${ config.ihe_api.url }/JobPosting.asmx/Logon?sUploaderID=${ loginId }&sPassword=${ password }` )
				.then( ( response ) => {
					let parser = new xml2js.Parser();

					parser.parseString( response.text, ( err, parsed ) => {
						if ( err ) {
							return reject( err );
						}

						return resolve( parsed );
					} );
				} )
				.catch( ( error ) => {
					return reject( error );
				} );
		} );
	},

	getProducts ( token ) {
		return new Promise( ( resolve, reject ) => {
			superagent.get( `${ config.ihe_api.url }/JobPosting.asmx/GetAvailableProducts?uiSecurityToken=${ token }` )
				.then( ( response ) => {
					let parser = new xml2js.Parser( {
						'explicitChildren' : true,
						'charsAsChildren'  : true
					} );

					parser.parseString( response.text, ( err, parsed ) => {
						if ( err ) {
							return reject( err );
						}

						return resolve( parsed );
					} );
				} )
				.catch( ( error ) => {
					return reject( error );
				} );
		} );
	},

	getCategories ( token ) {
		return new Promise( ( resolve, reject ) => {
			superagent.get( `${ config.ihe_api.url }/JobPosting.asmx/GetCategories?uiSecurityToken=${ token }` )
				.then( ( response ) => {
					let parser = new xml2js.Parser( {
						'explicitChildren' : true,
						'charsAsChildren'  : true
					} );

					parser.parseString( response.text, ( err, parsed ) => {
						if ( err ) {
							return reject( err );
						}

						return resolve( parsed );
					} );
				} )
				.catch( ( error ) => {
					return reject( error );
				} );
		} );
	},

	getCategoryTerms ( categoryId, token ) {
		return new Promise( ( resolve, reject ) => {
			superagent.get( `${ config.ihe_api.url }/JobPosting.asmx/GetHierarchicalCategoryTerms?uiSecurityToken=${ token }&sCategoryID=${ categoryId }` )
				.then( ( response ) => {
					let parser = new xml2js.Parser( {
						'explicitChildren' : true,
						'charsAsChildren'  : true
					} );

					parser.parseString( response.text, ( err, parsed ) => {
						if ( err ) {
							return reject( err );
						}

						return resolve( {
							'category_id' : categoryId,
							'terms' : parsed
						} );
					} );
				} )
				.catch( ( error ) => {
					return reject( error );
				} );
		} );
	}
};
