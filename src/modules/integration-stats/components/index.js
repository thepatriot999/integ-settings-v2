import React from 'react';
import ReactHighcharts from 'react-highcharts';

export default class IntegrationStats extends React.Component {

	componentWillMount () {
		this.setState( {
			'chart_data' : {
				'title' : {
					'text' : 'Integration Stats',
					'style' : {
						'font-weight' : '900',
						'color' : '#fff'
					}
				},
				'chart' : {
					'backgroundColor' : '#2a2b2c',
				},
				'credits' : {
					'href' : 'https://www.jobtarget.com',
					'text' : 'JobTarget.com'
				},
				'xAxis' : {
					'categories' : [ 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23 ]
				},
				'yAxis' : {
					'title' : {
						'text' : 'No. of Jobs',
						'style' : {
							'color' : '#fff'
						}
					},
				},
				'series' : [
					{
						'data' : this.normalizeHourlyData( [1,2,3,4] ),
						'name' : 'Failed',
						'color' : '#fff'
					},
					{
						'data' : this.normalizeHourlyData( [3,2,1,4,5] ),
						'name' : 'Success',
					}
				],
				'colors' : [
					'#f00',
					'rgb(124, 181, 236)'
				]
			}
		} );
	}

	componentDidMount () {
		this.refs.chart.showLoading().getChart();
	}

	normalizeHourlyData( hourlyData ) {
		if( hourlyData.length < 24 ) {
			let lacking = 24 - hourlyData.length;
			for( ; lacking > 0; lacking-- ) {
				hourlyData.push( 0 );
			}
		}

		return hourlyData;
	}

	render () {
		return (
			<div className="integration-stats">
				<div className="chart">
					<ReactHighcharts ref="chart" config={ this.state.chart_data } />
				</div>
			</div>
		);
	}
}
