export const REFRESH_PAGE = 'FILTER_CHANGED';

export function refreshPage () {
	return {
		'type' : REFRESH_PAGE
	};
}
