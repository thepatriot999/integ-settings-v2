import React from 'react';
import { Table,
	TableBody,
	TableFooter,
	TableHeader,
	TableHeaderColumn,
	TableRow,
	TableRowColumn
} from 'material-ui/Table';
import FlatButton from 'material-ui/FlatButton';
import FontIcon from 'material-ui/FontIcon';
import EditDialog from '../../../common/edit-dialog';
import DialogDropdown from '../../../common/dialog-dropdown';
import transmitterSource from '../../transmitter/sources';
import formatterSource from '../../formatter/sources';
import SettingsList from './settings-list';

export default class Settings extends React.Component {

	constructor ( props ) {
		super( props );

		this.state = this.updateStateFromProps( props );
	}

	componentWillMount () {
		this.loadData();
	}

	componentWillReceiveProps ( nextProps ) {
		let updatedState = this.updateStateFromProps( nextProps );

		this.setState( updatedState );
	}

	updateStateFromProps ( props ) {
		let newState = Object.assign( {}, this.state );

		if ( props.data ) {
			newState[ 'data' ] = props.data;
		}

		if ( props.mode ) {
			newState[ 'mode' ] = props.mode;
		}

		return newState;
	}

	loadData () {
		if ( this.props.mode === 'formatter' ) {
			this.loadFormatters();
		} else {
			this.loadTransmitters();
		}
	}

	loadFormatters () {
		formatterSource.getFormatters()
			.then( ( formatters ) => {
				this.setState( {
					'formatters' : formatters
				} );
			} )
			.catch( ( error ) => {
				console.log( error );
			} );
	}

	loadTransmitters () {
		transmitterSource.getTransmitters()
			.then( ( transmitters ) => {
				this.setState( {
					'transmitters' : transmitters
				} );
			} )
			.catch( ( error ) => {
				console.log( error );
			} );
	}

	addNewSettingClickedHandler () {
		this.refs.add_dialog.showAddDialog( 'double' );
	}

	showDropDown () {
		if ( this.props.mode === 'formatter' ) {
			this.refs.dialog_dropdown.show( this.state.formatters );
		} else {
			this.refs.dialog_dropdown.show( this.state.transmitters );
		}
	}

	optionChanged ( id ) {
		if ( this.props.classChanged && typeof this.props.classChanged == 'function' ) {
			this.props.classChanged( id );
		}
	}

	dataSaveHandler ( data ) {
		if ( this.props.addOptionClosed && typeof this.props.addOptionClosed == 'function' ) {
			this.props.addOptionClosed( data );
		}
	}

	render () {
		let tableContent = '';
		let data = this.state.data;
		let listData = [];
		let addNewStyle = {
			'display' : 'block'
		};

		if ( !data.formatter && !data.transmitter ) {
			data = {
				'id' : 'xxx',
				'settings' : []
			};

			addNewStyle = {
				'display' : 'none'
			};
		} else {
			if ( this.props.mode === 'formatter' ) {
				data.id = data.formatter.format_id;

				if ( data.formatter_settings ) {
					listData = data.formatter_settings;
				}
			} else {
				data.id = data.transmitter.transmit_id;

				if ( data.transmitter_settings ) {
					listData = data.transmitter_settings;
				}
			}
		}

		return (
			<div className="setting">
				<div className="setting-header">
					<div className="setting-main">
						Job{ this.state.mode }ter_{ data.id } <span onTouchTap={ this.showDropDown.bind( this ) }> (change) </span>
					</div>
					<div className="add-new">
						<FlatButton style={ addNewStyle } label="Add New" icon={ <FontIcon className="fa fa-plus" /> } onTouchTap={ this.addNewSettingClickedHandler.bind( this ) } />
					</div>
				</div>

				<SettingsList data={ listData } siteId={ this.props.site.site_id } />
				<EditDialog saveHandler={ this.dataSaveHandler.bind( this ) } ref="add_dialog" />
				<DialogDropdown optionSelectedHandler={ this.optionChanged.bind( this ) } currentOption={ data } ref="dialog_dropdown" />
			</div>
		);
	}
}
