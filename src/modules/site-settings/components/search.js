import React from 'react';
import source from '../sources';

export default class Search extends React.Component {

	componentWillMount () {
		this.setState( {
			'show_results'  : false,
			'search_string' : '',
			'sites'		: [],
			'selected'      : 0
		} );
	}

	searchStringChanged ( evt ) {
		if ( evt.target.value === '' ) {
			this.setState( {
				'show_results'  : false,
				'selected'  	: 0,
				'search_string' : ''
			} );
		} else {
			let state = {
				'search_string' : evt.target.value,
				'selected'  	: 0,
				'show_results'  : true
			};

			source.searchSite( evt.target.value )
				.then( ( response ) => {
					this.setState( Object.assign( {}, state, {
						'sites' : response
					} ) );
				} )
				.catch( ( error ) => {
					console.log( error );
				} );
		}
	}

	keyPressed ( evt ) {
		let keyCode = evt.nativeEvent.keyCode;

		if ( keyCode === 38 || keyCode === 40 ) {
			/*
			 * Prevent default behavior
			 */
			evt.nativeEvent.preventDefault();

			let selected = this.state.selected;

			if ( keyCode === 38 && selected > 0 ) {
				selected--;
			} else if ( keyCode === 38 && selected === 0 ) {
				selected = this.state.sites.length;
			} else if ( keyCode === 40 && selected < this.state.sites.length ) {
				selected++;
			} else if ( keyCode === 40 && selected >= this.state.sites.length ) {
				selected = 0;
			}

			this.setState( {
				'selected' : selected
			} );
		} else if ( keyCode === 13 ) {
			evt.nativeEvent.preventDefault();

			this.setSelectedSite( this.state.sites[ this.state.selected -1 ] )();
		}
	}

	setSelectedSite ( site ) {
		return () => {
			this.setState( {
				'selected_site' : site,
				'show_results'  : false,
				'search_string' : `${ site.site_id } - ${ site.site_name }`
			} );

			this.triggerSelectedSiteChanged( site );
		};
	}

	triggerSelectedSiteChanged ( site ){
		if ( this.props.onSelectedSiteChanged && typeof this.props.onSelectedSiteChanged == 'function' ) {
			this.props.onSelectedSiteChanged( site );
		}
	}

	render () {
		let resultsContainerClass = '';
		let containerClassName = '';
		let sites = '';

		if ( this.state.show_results && this.state.sites ) {
			resultsContainerClass = 'show';

			sites = this.state.sites.map( ( site, index ) => {
				let itemClass = 'result-item';

				/*
				 * We are going to check if the current highlighted option is the same as the one we're iterating
				 */
				if ( ( index + 1 ) === this.state.selected ) {
					itemClass = 'result-item result-item-selected';
				}

				return <div onTouchTap={ this.setSelectedSite( site ).bind( this ) } key={ `site-item${ site.site_id }` } className={ itemClass }>
						{ site.site_id } - { site.site_name }
					</div>;
			} );
		}

		if ( this.props.containerClassName ) {
			containerClassName = this.props.containerClassName;
		}

		return (
			<div className={ `search-box ${ containerClassName }` }>
				<span> Search </span>
				<div className="search-container">
					<input value={ this.state.search_string } onKeyDown={ this.keyPressed.bind( this ) } onChange={ this.searchStringChanged.bind( this ) } type="text" />
					<div className={ `results-container ${ resultsContainerClass }` }>
						{ sites }
					</div>
				</div>
			</div>
		);
	}
}
