import React from 'react';
import Search from './search';
import Settings from './settings';
import { Table,
	TableBody,
	TableFooter,
	TableHeader,
	TableHeaderColumn,
	TableRow,
	TableRowColumn
} from 'material-ui/Table';
import FlatButton from 'material-ui/FlatButton';
import FontIcon from 'material-ui/FontIcon';
import source from '../sources';
import store from '../.././../store';

export default class SiteSettings extends React.Component {

	componentWillMount () {
		this.setState( {
			'selected_site'    : false,
			'transmitter_data' : {
				'transmitter_settings' : []
			},
			'formatter_data' : {
				'formatter_settings' : []
			}
		} );

		store.unsubscribe = store.subscribe( () => {
			this.selectedSiteChanged( this.state.selected_site );
		} );
	}

	selectedSiteChanged ( site ) {
		let newState = {
			'formatter_data'   : {},
			'transmitter_data' : {},
			'selected_site'    : {}
		};

		newState[ 'selected_site' ] = site;
		/*
		 * Re-set the current values of transmitter and formatter
		 */

		let promiseFormatter = source.getSiteFormatterSettings( site.site_id );
		let promiseTransmitter = source.getSiteTransmitterSettings( site.site_id );

		Promise.all( [ promiseFormatter, promiseTransmitter ] )
			.then( ( responses ) => {
				if ( responses[ 0 ].formatter ) {
					newState[ 'formatter_data' ] = responses[ 0 ];
				}

				if ( responses[ 1 ].transmitter ) {
					newState[ 'transmitter_data' ] = responses[ 1 ];
				}

				this.setState( newState );
			} )
			.catch( ( error ) => {
				console.log( error );
			} );
	}

	updateSiteTransmitter ( id ){
		source.updateSiteTransmitter( this.state.selected_site.site_id, id )
			.then( ( response ) => {
				this.selectedSiteChanged( this.state.selected_site );
			} )
			.catch( ( error ) => {
				console.log( error );
			} );
	}

	updateSiteFormatter ( id ){
		source.updateSiteFormatter( this.state.selected_site.site_id, id )
			.then( ( response ) => {
				this.selectedSiteChanged( this.state.selected_site );
			} )
			.catch( ( error ) => {
				console.log( error );
			} );
	}

	addTransmitterSetting ( data ) {
		let requestData = {
			'transmit_site_id' : this.state.transmitter_data.transmitter.transmit_site_id,
			'name' 		   : data.name,
			'value' 	   : data.value
		};

		source.addSiteTransmitterSetting( requestData )
			.then( ( response ) => {
				this.selectedSiteChanged( this.state.selected_site );
			} )
			.catch( ( error ) => {
				console.log( error );
			} );
	}

	addFormatterSetting ( data ) {
		let requestData = {
			'format_site_id' : this.state.formatter_data.formatter.format_site_id,
			'name' 		 : data.name,
			'value' 	 : data.value
		};

		source.addSiteFormatterSetting( requestData )
			.then( ( response ) => {
				this.selectedSiteChanged( this.state.selected_site );
			} )
			.catch( ( error ) => {
				console.log( error );
			} );
	}

	render () {
		let tableContent = '';

		/*
		 * This will display the currently selected site and it's transmitter and formatter info.
		 */
		let selectedSiteDisplay = (
			<div className="details">
				<div className="selected-site-display">
					<h3> { `${  this.state.selected_site.site_name }` } </h3>
					<span className="site-id"> { this.state.selected_site.site_id } </span>
				</div>

				<Settings site={ this.state.selected_site } addOptionClosed={ this.addTransmitterSetting.bind( this ) } classChanged={ this.updateSiteTransmitter.bind( this ) } mode="transmitter" data={ this.state.transmitter_data } type="Transmit" />
				<Settings site={ this.state.selected_site } addOptionClosed={ this.addFormatterSetting.bind( this ) } classChanged={ this.updateSiteFormatter.bind( this ) } mode="formatter" data={ this.state.formatter_data } type="Format" />
			</div>
		);

		/*
		 * If the user has not selected a site, we can't really display the selected site
		 */
		if ( !this.state.selected_site ) {
			selectedSiteDisplay = '';
		}

		return (
			<div className="site-settings">
				<h2> Site Integration Settings </h2>
				<Search onSelectedSiteChanged={ this.selectedSiteChanged.bind( this ) } />

				{ selectedSiteDisplay }
			</div>
		);
	}
}
