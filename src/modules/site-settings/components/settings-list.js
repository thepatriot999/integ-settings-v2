import React from 'react';
import { Table,
	TableBody,
	TableFooter,
	TableHeader,
	TableHeaderColumn,
	TableRow,
	TableRowColumn
} from 'material-ui/Table';
import moment from 'moment';
import IconButton from 'material-ui/IconButton';
import EditDialog from '../../../common/edit-dialog';
import YesNoDialog from '../../../common/dialog';
import source from '../sources';
import { refreshPage } from '../actions';
import store from '../.././../store';

export default class Transmitter extends React.Component {

	componentWillMount () {
		this.setState( {
			'data'    : [],
			'site_id' : ''
		} );
	}

	componentWillReceiveProps ( nextProps ) {
		let updatedState = {};

		if ( nextProps.siteId ) {
			updatedState.site_id = nextProps.siteId;
		}

		if ( nextProps.data ) {
			updatedState.data = nextProps.data;
		}

		this.setState( updatedState );
	}

	editClickedHandler ( item ) {
		return () => {
			this.refs.edit_dialog.showEditDialog( Object.assign( {}, item ), 'double' );
		};
	}

	deleteClickedHandler ( item ) {
		return () => {
			this.refs.yes_no_dialog.show( 'Delete', 'Are you sure you want to delete this record?', item );
		};
	}

	editHandler ( data ) {
		if ( data.format_setting_id ) {
			source.updateSiteFormatterSetting( data )
				.then( ( response ) => {
					store.dispatch( refreshPage() );
				} )
				.catch( ( error ) => {
					console.log( error );
				} );
		} else {
			source.updateSiteTransmitterSetting( data )
				.then( ( response ) => {
					store.dispatch( refreshPage() );
				} )
				.catch( ( error ) => {
					console.log( error );
				} );
		}
	}

	deleteHandler () {
		let data = this.refs.yes_no_dialog.state.id;

		if ( data.transmit_setting_id ) {
			source.deleteSiteTransmitterSetting( data.transmit_setting_id )
				.then( ( response ) => {
					store.dispatch( refreshPage() );
				} )
				.catch( ( error ) => {
					console.log( error );
				} );
		} else {
			source.deleteSiteFormatterSetting( data.format_setting_id )
				.then( ( response ) => {
					store.dispatch( refreshPage() );
				} )
				.catch( ( error ) => {
					console.log( error );
				} );
		}
	}

	updateData ( data ) {
		this.setState( {
			'data' : data
		} );
	}

	render () {
		let tableContent = this.state.data.map( ( row, index ) => {
			return <TableRow key={ index }>
				<TableRowColumn className="col-id">
					{ row.transmit_setting_id ? row.transmit_setting_id : row.format_setting_id }
				</TableRowColumn>
				<TableRowColumn className="col-title">
					{ row.name }
				</TableRowColumn>
				<TableRowColumn className="col-title">
					{ row.value }
				</TableRowColumn>
				<TableRowColumn className="col-date">
					{ moment( row.added ).format( 'Y-MM-DD' ) }
				</TableRowColumn>
				<TableRowColumn className="col-date">
					{ row.updated ? moment( row.updated ).format( 'Y-MM-DD' ) : '' }
				</TableRowColumn>
				<TableRowColumn className="col-date">
					{ row.removed ? moment( row.removed ).format( 'Y-MM-DD' ) : '' }
				</TableRowColumn>
				<TableRowColumn className="col-active">
					{ row.active }
				</TableRowColumn>
				<TableRowColumn>
					<IconButton onTouchTap={ this.editClickedHandler( row ).bind( this ) } iconClassName="fa fa-pencil-square-o"/>
					<IconButton onTouchTap={ this.deleteClickedHandler( row ).bind( this ) } iconClassName="fa fa-times"/>
				</TableRowColumn>
			</TableRow>;
		} );

		return (
			<div className="settings-list">
				<div className="table-list">
					<Table selectable={ false }>
						<TableHeader adjustForCheckbox={ false } displaySelectAll={ false }>
							<TableRow>
								<TableHeaderColumn className="col-id"> Transmit Setting ID </TableHeaderColumn>
								<TableHeaderColumn className="col-title"> Name </TableHeaderColumn>
								<TableHeaderColumn className="col-title"> Value </TableHeaderColumn>
								<TableHeaderColumn className="col-date"> Added </TableHeaderColumn>
								<TableHeaderColumn className="col-date"> Updated </TableHeaderColumn>
								<TableHeaderColumn className="col-date"> Removed </TableHeaderColumn>
								<TableHeaderColumn className="col-active"> Active </TableHeaderColumn>
								<TableHeaderColumn> Actions </TableHeaderColumn>
							</TableRow>
						</TableHeader>
						<TableBody displayRowCheckbox={ false }>
							{ tableContent }
						</TableBody>
					</Table>
				</div>
				<EditDialog saveHandler={ this.editHandler.bind( this ) } ref="edit_dialog" />
				<YesNoDialog yesHandler={ this.deleteHandler.bind( this ) } ref="yes_no_dialog" />
			</div>
		);
	}
}
