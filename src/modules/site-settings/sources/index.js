import http from '../../../common/http';

export default {
	searchSite ( queryString ) {
		return http.get( `/site/search/${ queryString }` );
	},

	getSiteFormatterSettings ( siteId ) {
		return http.get( `/formatter/settings/${ siteId }` );
	},

	getSiteTransmitterSettings ( siteId ) {
		return http.get( `/transmitter/settings/${ siteId }` );
	},

	updateSiteTransmitter ( siteId, transmitId ) {
		return http.post( `/transmitter/settings/${ siteId }/${ transmitId }` );
	},

	updateSiteFormatter ( siteId, formatId ) {
		return http.post( `/formatter/settings/${ siteId }/${ formatId }` );
	},

	addSiteTransmitterSetting ( data ) {
		return http.post( '/transmitter/setting', data );
	},

	addSiteFormatterSetting ( data ) {
		return http.post( '/formatter/setting', data );
	},

	updateSiteTransmitterSetting ( data ) {
		return http.put( '/transmitter/setting', data );
	},

	updateSiteFormatterSetting ( data ) {
		return http.put( '/formatter/setting', data );
	},

	deleteSiteTransmitterSetting ( id ) {
		return http.del( `/transmitter/setting/${ id }` );
	},

	deleteSiteFormatterSetting ( id ) {
		return http.del( `/formatter/setting/${ id }` );
	}
}
