import http from '../../../common/http';

export default {
	getFormatters () {
		return http.get( '/formatter' );
	},

	save ( data ) {
		return http.post( '/formatter', data );
	},

	update ( data ) {
		return http.put( '/formatter', data );
	},

	del ( id ) {
		return http.del( `/formatter/${ id }` );
	}
}
