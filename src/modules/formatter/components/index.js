import React from 'react';
import { Table,
	TableBody,
	TableFooter,
	TableHeader,
	TableHeaderColumn,
	TableRow,
	TableRowColumn
} from 'material-ui/Table';
import moment from 'moment';
import FontIcon from 'material-ui/FontIcon';
import IconButton from 'material-ui/IconButton';
import FlatButton from 'material-ui/FlatButton';
import EditDialog from '../../../common/edit-dialog';
import YesNoDialog from '../../../common/dialog';
import source from '../sources';

export default class Formatter extends React.Component {

	componentWillMount () {
		this.setState( {
			'data' : []
		} );

		this.loadFormatters();
	}

	loadFormatters () {
		source.getFormatters()
			.then( ( formatters ) => {
				this.setState( {
					'data' : formatters
				} );
			} )
			.catch( ( error ) => {
				console.log( error );
			} );
	}

	editClickedHandler ( item ) {
		return () => {
			this.refs.edit_dialog.showEditDialog( Object.assign( {}, item ) );
		};
	}

	deleteClickedHandler ( item ) {
		return () => {
			this.refs.yes_no_dialog.show( 'Delete', 'Are you sure you want to delete this record?', item.format_id );
		};
	}

	addNewClickedHandler () {
		this.refs.add_dialog.showAddDialog();
	}

	saveHandler ( data ) {
		source.save( data )
			.then( ( status ) => {
				this.loadFormatters();
			} )
			.catch( ( error ) => {
				console.log( error );
			} );
	}

	editHandler ( data ) {
		source.update( data )
			.then( ( status ) => {
				this.loadFormatters();
			} )
			.catch( ( error ) => {
				console.log( error );
			} );
	}

	deleteHandler () {
		let id = this.refs.yes_no_dialog.state.id;

		source.del( id )
			.then( ( status ) => {
				this.loadFormatters();
			} )
			.catch( ( error ) => {
				console.log( error );
			} );
	}

	render () {
		let tableContent = this.state.data.map( ( row, index ) => {
			return <TableRow key={ index }>
				<TableRowColumn className="col-id">
					{ row.format_id }
				</TableRowColumn>
				<TableRowColumn className="col-title">
					{ row.title }
				</TableRowColumn>
				<TableRowColumn className="col-date">
					{ moment( row.added ).format( 'Y-MM-DD' ) }
				</TableRowColumn>
				<TableRowColumn className="col-date">
					{ row.updated ? moment( row.updated ).format( 'Y-MM-DD' ) : '' }
				</TableRowColumn>
				<TableRowColumn className="col-date">
					{ row.removed ? moment( row.removed ).format( 'Y-MM-DD' ) : '' }
				</TableRowColumn>
				<TableRowColumn className="col-active">
					{ row.active }
				</TableRowColumn>
				<TableRowColumn>
					<IconButton onTouchTap={ this.editClickedHandler( row ).bind( this ) } iconClassName="fa fa-pencil-square-o"/>
					<IconButton onTouchTap={ this.deleteClickedHandler( row ).bind( this ) } iconClassName="fa fa-times"/>
				</TableRowColumn>
			</TableRow>;
		} );

		return (
			<div className="main-content">
				<h1> Formatters </h1>

				<div className="add-new">
					<FlatButton label="Add New" icon={ <FontIcon className="fa fa-plus" /> } onTouchTap={ this.addNewClickedHandler.bind( this ) } />
				</div>

				<div className="table-list">
					<Table selectable={ false }>
						<TableHeader adjustForCheckbox={ false } displaySelectAll={ false }>
							<TableRow>
								<TableHeaderColumn className="col-id"> Format ID </TableHeaderColumn>
								<TableHeaderColumn className="col-title"> Title </TableHeaderColumn>
								<TableHeaderColumn className="col-date"> Added </TableHeaderColumn>
								<TableHeaderColumn className="col-date"> Updated </TableHeaderColumn>
								<TableHeaderColumn className="col-date"> Removed </TableHeaderColumn>
								<TableHeaderColumn className="col-active"> Active </TableHeaderColumn>
								<TableHeaderColumn> Actions </TableHeaderColumn>
							</TableRow>
						</TableHeader>
						<TableBody displayRowCheckbox={ false }>
							{ tableContent }
						</TableBody>
					</Table>
				</div>
				<EditDialog saveHandler={ this.editHandler.bind( this ) } ref="edit_dialog" />
				<EditDialog saveHandler={ this.saveHandler.bind( this ) } ref="add_dialog" />
				<YesNoDialog yesHandler={ this.deleteHandler.bind( this ) } ref="yes_no_dialog" />
			</div>
		);
	}
}
