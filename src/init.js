import injectTapEventPlugin from 'react-tap-event-plugin';

/*
 * Fix onTouchTap errors
 */
injectTapEventPlugin();
