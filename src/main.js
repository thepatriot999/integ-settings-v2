import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { Router, Route, useRouterHistory, IndexRoute, IndexRedirect } from 'react-router';
import createHistory from 'history/lib/createHashHistory';
import store from './store';
import injectTapEventPlugin from 'react-tap-event-plugin';

injectTapEventPlugin();

/*
 * Initialize router
 */
const appHistory = useRouterHistory( createHistory )( { 'queryKey' : false } );

/*
 * Include styles
 */
import '!style!css!sass!./styles/main.scss';
import '!style!css!../font-awesome/css/font-awesome.min.css';

/*
 * Include auth helper
 */
import Auth from './auth.js';

/*
 * Include modules
 */
import Layout from './layout';
import Main from './modules/main/components/';
import Login from './modules/login/components/';
import Transmitter from './modules/transmitter/components/';
import Formatter from './modules/formatter/components/';
import SiteSettings from './modules/site-settings/components/';
import JobStatus from './modules/job-status/components/';
import InsideHigherEd from './modules/inside-higher-ed/components/';
import IntegrationStats from './modules/integration-stats/components/';

ReactDOM.render( <Provider store={ store }>
		<Router history={ appHistory }>
			<Route path="/" component={ Layout }>
				<IndexRedirect to="job-status" />
				<Route path="home" onEnter={ Auth.requireAuthentication } component={ Main } />
				<Route path="transmitter" onEnter={ Auth.requireAuthentication } component={ Transmitter } />
				<Route path="formatter" onEnter={ Auth.requireAuthentication } component={ Formatter } />
				<Route path="site-settings" onEnter={ Auth.requireAuthentication } component={ SiteSettings } />
				<Route path="job-status" onEnter={ Auth.requireAuthentication } component={ JobStatus } />
				<Route path="integration-stats" onEnter={ Auth.requireAuthentication } component={ IntegrationStats } />
			</Route>
			<Route path="others" component={ Layout }>
				<Route path="inside-higher-ed" onEnter={ Auth.requireAuthentication } component={ InsideHigherEd } />
			</Route>
			<Route path="/lokin" onEnter={ Auth.checkAuthenticated } component={ Login } />
		</Router>
	</Provider>,
	document.getElementById( 'root' )
);
