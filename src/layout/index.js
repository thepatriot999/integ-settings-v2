import React from 'react';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import Header from './header';

class Layout extends React.Component {

	render () {
		return (
			<MuiThemeProvider>
				<div className="layout">
					<Header />
					{ this.props.children }
				</div>
			</MuiThemeProvider>
		);
	}
}

export default Layout;
