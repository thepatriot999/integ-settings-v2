import React from 'react';
import { Link } from 'react-router';
import MenuItem from 'material-ui/MenuItem';
import Drawer from 'material-ui/Drawer';

export default class Header extends React.Component {

	componentWillMount () {
		this.setState( {
			'nav_toggle' : false,
			'active_option' : '',
			'nav_options' : {
				'Distribution Settings' : {
					'type' : 'group',
					'sub_options' : [
						{
							'label' : 'Transmitter',
							'link' : 'transmitter'
						},
						{
							'label' : 'Formatter',
							'link' : 'formatter'
						},
						{
							'label' : 'Site Settings',
							'link' : 'site-settings'
						}
					]
				},
				'Job Status' : {
					'type' : 'link',
					'link' : 'job-status'
				},
				'Stats' : {
					'type' : 'link',
					'link' : 'integration-stats'
				},
				'Others' : {
					'type' : 'group',
					'sub_options' : [
						{
							'label' : 'Inside Higher Ed',
							'link' : 'others/inside-higher-ed'
						},
						{
							'label' : 'Chronicle',
							'link' : 'others/chronicle'
						}
					]
				}

			}
		} );
	}

	resetActiveOption () {
		this.setState( {
			'active_option' : ''
		} );
	}

	toggleNav () {
		this.setState( {
			'nav_toggle' : !this.state.nav_toggle
		} );
	}

	optionClicked ( option ) {
		return () => {
			this.setState( {
				'active_option' : option
			} )
		};
	}

	createOptions () {
		let navOptions = this.state.nav_options;

		if ( this.state.active_option === '' ) {
			return Object.keys( navOptions ).map( ( label ) => {
				let option = navOptions[ label ];

				if ( option.type === 'link' ) {
					return <Link key={ `link-to${ label }` } to={ option.link }> <MenuItem key={ label }> { label } </MenuItem></Link>;
				}

				return <MenuItem key={ label } onTouchTap={ this.optionClicked( label ).bind( this ) }> { label } </MenuItem>;
			} );
		} else {
			let subOptions = navOptions[ this.state.active_option ].sub_options;
			let composedSubOptions = [
				<MenuItem onTouchTap={ this.resetActiveOption.bind( this ) } key="back">
					<i className="fa fa-arrow-circle-left" aria-hidden="true"></i>
					Back
				</MenuItem>
			];

			let mappedSubOptions = subOptions.map( ( option ) => {
				return <Link key={ `link-to${ option.label }` } to={ option.link }> <MenuItem key={ option.label }> { option.label } </MenuItem> </Link>;
			} );

			composedSubOptions = composedSubOptions.concat( mappedSubOptions );

			return composedSubOptions;
		}
	}

	render () {
		let options = this.createOptions();

		return (
			<div className="header">
				<div className="jobtarget-logo">
					<svg version="1.1" id="Layer_2" xmlns="http://www.w3.org/2000/svg" x="0px" y="0px" viewBox="0 0 194.6 44.3">
						<g>
							<g>
									<path fill="#FFFFFF" d="M22.4,2.3C11.5,2.9,2.2,12.2,1.6,23.1C1,34.1,9.3,42.4,20.2,41.8c10.9-0.6,20.2-9.9,20.8-20.8
									C41.7,10,33.3,1.7,22.4,2.3z M33.5,22.3c-0.4,8-7.3,14.9-15.3,15.3c-8,0.4-14.2-5.7-13.7-13.7c0.4-8,7.3-14.9,15.3-15.3
									C27.8,8.1,33.9,14.2,33.5,22.3z"></path>
									<path fill="#FFFFFF" d="M18.2,13.7C12.4,14,7.4,19,7.1,24.8c-0.3,5.8,4.1,10.3,9.9,9.9c5.8-0.3,10.8-5.3,11.1-11.1
									C28.5,17.9,24.1,13.4,18.2,13.7z M16.2,32.3c-4,0.2-7.1-2.8-6.8-6.8c0.2-4,3.6-7.4,7.6-7.6c4-0.2,7.1,2.8,6.8,6.8
									C23.6,28.7,20.2,32.1,16.2,32.3z"></path>
									<path fill="#FFFFFF" d="M16.4,22.4c-1.7,0.1-3.2,1.6-3.3,3.3c-0.1,1.7,1.2,3.1,3,3c1.7-0.1,3.2-1.6,3.3-3.3
									C19.4,23.7,18.1,22.3,16.4,22.4z"></path>
								</g>
							<g>
								<path fill="#FFFFFF" d="M56.2,25.3c0,3.5-0.9,6.8-6.3,6.8c-3.1,0-5.4-1.4-6-4.4l4-0.9c0.1,0.9,1,1.6,1.9,1.6c2,0,2.1-2.2,2.1-3.6
								V12.7h4.3V25.3z"></path>
								<path fill="#FFFFFF" d="M67.8,12.3c6.2,0,10.6,3.9,10.6,9.9c0,6-4.5,9.9-10.6,9.9c-6.2,0-10.6-3.9-10.6-9.9
								C57.2,16.1,61.7,12.3,67.8,12.3z M67.8,28.2c3.7,0,6.1-2.6,6.1-6.1c0-3.5-2.4-6.1-6.1-6.1c-3.7,0-6.1,2.6-6.1,6.1
								C61.7,25.7,64.1,28.2,67.8,28.2z"></path>
								<path fill="#FFFFFF" d="M79.5,12.7h7.3c3.5,0,7.2,0.7,7.2,4.8c0,2.1-1.4,3.5-3.4,4.1v0.1c2.6,0.3,4.3,2.2,4.3,4.6
								c0,3.9-3.9,5.2-7.4,5.2h-8.1V12.7z M83.8,20.3H87c1.3,0,2.7-0.5,2.7-2c0-1.6-1.6-2-2.9-2h-3V20.3z M83.8,28h3.9
								c1.4,0,2.9-0.6,2.9-2.2c0-1.8-2.1-2.1-3.5-2.1h-3.3V28z"></path>
								<path fill="#FFFFFF" d="M100.5,16.4h-5.6v-3.7h15.6v3.7h-5.6v15.1h-4.3V16.4z"></path>
								<path fill="#FFFFFF" d="M114.1,12.7h3.6l8.6,18.8h-4.9l-1.7-4H112l-1.6,4h-4.8L114.1,12.7z M115.8,18.2l-2.4,5.8h4.8L115.8,18.2z"></path>
								<path fill="#FFFFFF" d="M127.1,12.7h7.6c4,0,7.4,1.3,7.4,5.7c0,2.6-1.5,4.7-4.3,5.2l5,7.9h-5.2l-4.1-7.5h-2.1v7.5h-4.3V12.7z
								M131.5,20.5h2.6c1.6,0,3.6-0.1,3.6-2.1c0-1.9-1.8-2.2-3.4-2.2h-2.8V20.5z"></path>
								<path fill="#FFFFFF" d="M161.7,30.2c-2.6,1.3-5.4,1.8-8.3,1.8c-6.2,0-10.6-3.9-10.6-9.9c0-6,4.5-9.9,10.6-9.9
								c3.1,0,5.9,0.7,7.8,2.4l-3.1,3.2c-1.2-1.2-2.6-1.8-4.8-1.8c-3.7,0-6.1,2.6-6.1,6.1c0,3.5,2.4,6.1,6.1,6.1c1.9,0,3.3-0.5,4.1-0.9
								v-3.1H154v-3.8h7.7V30.2z"></path>
								<path fill="#FFFFFF" d="M162.9,12.7h13.4v3.8h-9v3.5h9v3.8h-9v3.8h9v3.8h-13.4V12.7z"></path>
								<path fill="#FFFFFF" d="M182.9,16.4h-5.6v-3.7h15.6v3.7h-5.6v15.1h-4.3V16.4z"></path>
							</g>
						</g>
					</svg>
				</div>
				<div className='drop-down'>
					<div onClick={ this.toggleNav.bind( this ) } className="btn">
						<i className="fa fa-server fa-lg"></i>
					</div>
				</div>
			<Drawer className="menu" containerClassName="container" openSecondary={ true } onRequestChange={ ( open ) => { this.toggleNav() } } open={ this.state.nav_toggle } ref="nav">
				{ options }
			</Drawer>
			</div>
		);
	}
}
