import http from 'common/http';
import config from './config';

function login ( user ) {
	return http.post( '/authenticate', user )
		.then( ( res ) => {
			localStorage.setItem( 'jwt', res.token );
		} );
}

function logOut () {
	localStorage.removeItem( 'jwt' );
	window.location = config.default_page;
}

function isAuthenticated () {
	if ( localStorage.getItem( 'jwt' ) ) {
		return true;
	}

	/* Should return false, we are disabling authentication for now */
	return true;
}

function requireAuthentication ( nextState, replace ) {
	if ( !isAuthenticated() ) {
		replace( {
			'pathname' : '/login',
			'state'    : {
				'nextPathName' : nextState.location.pathname
			}
		} );
	}
}

function checkAuthenticated ( nextState, replace ) {
	if ( isAuthenticated() ) {
		replace( {
			'pathname' : '/',
			'state'    : {
				'nextPathName' : nextState.location.pathname
			}
		} );
	}
}

function getToken () {
	return localStorage.getItem( 'jwt' );
}

export default {
	login,
	logOut,
	isAuthenticated,
	requireAuthentication,
	checkAuthenticated,
	getToken
};
