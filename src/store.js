import { createStore, combineReducers } from 'redux';
import mainReducer from './modules/main/reducers';
import jobStatusReducer from './modules/job-status/reducers';

const store = createStore(
	combineReducers( {
		mainReducer,
		jobStatusReducer
	} )
);

export default store;
