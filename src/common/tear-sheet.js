import React from 'react';

export default class TearSheet extends React.Component {
	render () {
		return (
			<div className={ `tear-sheet ${ this.props.className }` }>
				{ this.props.children }
			</div>
		);
	}
}
