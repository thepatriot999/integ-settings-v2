import React from 'react';
import Dialog from 'material-ui/Dialog';
import RaisedButton from 'material-ui/RaisedButton';

export default class MessageDialog extends React.Component {

	componentWillMount () {
		this.setState( {
			'show'    : false,
			'message' : '',
			'title'   : '',
			'id'   	  : ''
		} );
	}

	show ( title, message, id ) {
		this.setState( {
			'show'    : true,
			'message' : message,
			'title'   : title,
			'id'	  : id
		} );
	}

	close () {
		this.setState( {
			'show' : false
		} );
	}

	triggerYesHandler () {
		if ( this.props.yesHandler && typeof this.props.yesHandler == 'function' ) {
			this.props.yesHandler();
		}

		this.close();
	}

	render () {
		const actions = [
			<RaisedButton
				label="Yes"
				primary={ true }
				keyboardFocused={ true }
				onTouchTap={ this.triggerYesHandler.bind( this ) } />,
			<RaisedButton
				label="No"
				secondary={ true }
				keyboardFocused={ true }
				onTouchTap={ this.close.bind( this ) } />
		];

		return (
			<Dialog title={ this.state.title }
				actions={ actions }
				modal={ false }
				open={ this.state.show }
				onRequestClose={ this.close.bind( this ) }>
				{ this.state.message }
			</Dialog>
		);
	}
}
