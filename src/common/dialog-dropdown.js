import React from 'react';
import Dialog from 'material-ui/Dialog';
import RaisedButton from 'material-ui/RaisedButton';
import DropDownMenu from 'material-ui/DropDownMenu';
import MenuItem from 'material-ui/MenuItem';

export default class MessageDialog extends React.Component {

	componentWillMount () {
		this.setState( {
			'show' 		 : false,
			'list'		 : [],
			'current_option' : {
				'id' : 0
			},
		} )
	}

	componentWillReceiveProps ( nextProps ) {
		if ( nextProps.currentOption ) {
			this.setState( {
				'current_option' : nextProps.currentOption
			} );
		}
	}

	show ( list ) {
		this.setState( {
			'show' : true,
			'list' : list
		} );
	}

	close () {
		this.setState( {
			'show' : false
		} );
	}

	fireOptionSelected () {
		if ( this.props.optionSelectedHandler && typeof this.props.optionSelectedHandler == 'function' ) {
			this.props.optionSelectedHandler( this.state.current_option.id );
		}

		this.close();
	}

	dropDownValueChanged ( evt, index, value ) {
		let currentOption = this.state.current_option;

		currentOption.id = value;

		this.setState( {
			'current_option' : currentOption
		} );
	}

	render () {
		let btnStyle = {
			'marginRight' : '10px'
		};
		let dropDownStyle = {
			'width'  : '500px',
			'margin' : 'auto',
			'display' : 'block'
		};

		const actions = [
			<RaisedButton
				label="Save"
				primary={ true }
				keyboardFocused={ true }
				style={ btnStyle }
				onTouchTap={ this.fireOptionSelected.bind( this ) } />,
			<RaisedButton
				label="Cancel"
				secondary={ true }
				keyboardFocused={ true }
				onTouchTap={ this.close.bind( this ) } />
		];

		let items = this.state.list.map( ( item, indx, arr ) => {
			let id = item.format_id !== undefined ? item.format_id : item.transmit_id;
			let mode = item.format_id ? 'formatter' : 'transmitter';

			return <MenuItem value={ id } key={ `${ mode }_${ id }` } primaryText={ `${ id } - ${ item.title }` } />;
		} );

		return (
			<Dialog title="Choose"
				actions={ actions }
				modal={ false }
				open={ this.state.show }
				onRequestClose={ this.close.bind( this ) }>
				<DropDownMenu onChange={ this.dropDownValueChanged.bind( this ) } value={ this.state.current_option.id } autoWidth={ false } style={ dropDownStyle } maxHeight={400}>
					{ items }
				</DropDownMenu>
			</Dialog>
		);
	}
}
