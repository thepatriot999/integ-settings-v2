import superagent from 'superagent';
import config from '../config';
import auth from '../auth';

export default {
	'get' : ( url  ) => {
		return new Promise( ( resolve, reject ) => {
			superagent
				.get( `${config.api.url}${url}` )
				.set( 'Authorization', `Bearer ${ auth.getToken() } ` )
				.end( ( err, res ) => {
					if ( err ) {
						return reject( res.body );
					}

					return resolve( res.body );
				} );
		} );
	},

	'del' : ( url  ) => {
		return new Promise( ( resolve, reject ) => {
			superagent
				.delete( `${config.api.url}${url}` )
				.set( 'Authorization', `Bearer ${ auth.getToken() } ` )
				.end( ( err, res ) => {
					if ( err ) {
						return reject( res.body );
					}

					return resolve( res.body );
				} );
		} );
	},

	'post' : ( url, data ) => {
		return new Promise( ( resolve, reject ) => {
			superagent
				.post( `${config.api.url}${url}` )
				.set( 'Content-type: application/json' )
				.set( 'Authorization', `Bearer ${ auth.getToken() } ` )
				.send( data )
				.end( ( err, res ) => {
					if ( err ) {
						reject( res.body );
					}

					resolve( res.body );
				} );
		} );
	},

	'put' : ( url, data ) => {
		return new Promise( ( resolve, reject ) => {
			superagent
				.put( `${config.api.url}${url}` )
				.set( 'Content-type: application/json' )
				.set( 'Authorization', `Bearer ${ auth.getToken() } ` )
				.send( data )
				.end( ( err, res ) => {
					if ( err ) {
						reject( res.body );
					}

					resolve( res.body );
				} );
		} );
	}
};
