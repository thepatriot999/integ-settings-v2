import React from 'react';
import Dialog from 'material-ui/Dialog';
import TextField from 'material-ui/TextField';
import RaisedButton from 'material-ui/RaisedButton';
import FlatButton from 'material-ui/FlatButton';

export default class EditDialog extends React.Component {

	componentWillMount () {
		this.setState( {
			'mode' : 'Add New',
			'data' : {},
			'open' : false,
			'type' : 'single'
		} );
	}

	showEditDialog ( data, type ) {
		let component = this;
		let state = Object.assign({}, this.state, {
				'data' : data,
				'open' : true,
				'mode' : 'Update'
			} );

		if ( type ) {
			state[ 'type' ] = type;
		}

		this.setState( state );

		setTimeout( function () {
			component.focusMainInput();
		}, 0 );
	}

	showAddDialog ( type ) {
		let component = this;
		let state = Object.assign({}, this.state, {
				'data' : {},
				'open' : true,
				'mode' : 'Add New'
			} );

		if ( type ) {
			state[ 'type' ] = type;
		}

		this.setState( state );

		setTimeout( function () {
			component.focusMainInput();
		}, 0 );
	}

	focusMainInput () {
		if ( this.state.type === 'single' ) {
			this.refs.title.focus();
		} else {
			this.refs.name.focus();
		}
	}

	handleClose () {
		this.setState( {
			'open' : false
		} );
	}

	textValueChanged ( evt ) {
		let data = this.state.data;

		data[ evt.target.name ] = evt.target.value;

		this.setState( {
			'data' : data
		} );
	}

	triggerSaveHandler () {
		if ( this.props.saveHandler && typeof this.props.saveHandler == 'function' ) {
			this.props.saveHandler( this.state.data );
		}

		this.handleClose();
	}

	render () {
		const actions = [
			<RaisedButton
				label="Save"
				primary={ true }
				keyboardFocused={ true }
				onTouchTap={ this.triggerSaveHandler.bind( this ) } />,
			<FlatButton
				label="Close"
				secondary={ true }
				keyboardFocused={ true }
				onTouchTap={ this.handleClose.bind( this ) } />
		];

		let titleStyle = {
			'textAlign' : 'center'
		};

		let content = <TextField ref="title" name="title" fullWidth={ true } floatingLabelText="Title" value={ this.state.data.title } onChange={ this.textValueChanged.bind( this ) }/>;

		if ( this.state.type == 'double' ) {
			content = <div>
					<TextField ref="name" name="name" fullWidth={ true } floatingLabelText="Name" value={ this.state.data.name } onChange={ this.textValueChanged.bind( this ) }/>
					<TextField ref="value" name="value" fullWidth={ true } floatingLabelText="Value" value={ this.state.data.value } onChange={ this.textValueChanged.bind( this ) }/>
				</div>;
		}

		return (
			<Dialog
				title={ this.state.mode }
				modal={ false }
				actions={ actions }
				open={ this.state.open }
				titleStyle={ titleStyle }
				onRequestClose={ this.handleClose.bind( this ) }>
				{ content }
			</Dialog>
		);
	}
}
