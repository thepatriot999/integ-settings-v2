import React from 'react';
import Popover from 'material-ui/Popover';
import Checkbox from 'material-ui/Checkbox';

export default class DropDownButton extends React.Component {

	constructor ( props ) {
		super( props );

		this.state = this.setStateFromProps( props );
	}

	componentWillReceiveProps ( nextProps ) {
		let newState = this.setStateFromProps( nextProps );

		this.setState( newState );
	}
	
	setStateFromProps ( props ) {
		let state = Object.assign( {
			'status_popover_show' : false,
			'status_popover_anchor' : null,
			'anchor' : null,
			'items' : [],
			'checked_items' : []
		}, this.state );

		if ( !state ) {
			state = {};
		}

		if ( props.items ) {
			state.items = props.items;
		}

		if ( props.label ) {
			state.label = props.label;
		}

		if ( props.anchor ) {
			state.anchor = props.anchor;
		}

		return state;
	}

	showPopOver ( evt ) {
		this.setState( {
			'status_popover_show' : true,
			'status_popover_anchor' : evt.currentTarget
		} );
	}

	requestClose ( evt ) {
		this.setState( {
			'status_popover_show' : false
		} );
	}

	checkBoxChecked ( evt, val ) {
		let key = evt.target.value;

		let checkedItems = Object.assign( {}, this.state.checked_items );

		if ( this.state.checked_items[ key ] ) {
			checkedItems[ key ] = val;
		} else {
			checkedItems[ key ] = val;
		}

		this.setState( {
			'checked_items' : checkedItems
		} );

		setTimeout( ( function () {
			this.fireCheckedItemsChanged( this.formatCheckedItems( checkedItems ) );
		} ).bind( this ), 0 )
	}

	fireCheckedItemsChanged ( checkedItems ) {
		if ( this.props.checkedItemsChanged && typeof this.props.checkedItemsChanged == 'function' ) {
			this.props.checkedItemsChanged.call( this, checkedItems );
		}
	}

	formatCheckedItems ( checkedItems ) {
		let filtered = Object.keys( checkedItems ).filter( ( key ) => {
			return checkedItems[ key ];
		} );

		return filtered;
	}

	render () {
		let style = {
			minWidth : '120px'
		};

		let checkedItems = this.formatCheckedItems( this.state.checked_items );

		let items = this.state.items.map( ( item ) => {
			let checked = checkedItems.indexOf( item ) != -1;

			return <Checkbox
				checked={ checked }
				value={ item }
				onCheck={ this.checkBoxChecked.bind( this ) }
				key={ `checkbox-${ item }` }
				label={ item } />;
		} );

		return (
			<div style={ this.props.style } onTouchTap={ this.showPopOver.bind( this ) } className="drop-down-button">
				<div className="label-container">
					<div className="label">
						{ this.state.label }
					</div>
					<div className="sort-icon fa fa-sort-desc"></div>
				</div>
				<Popover
				style={ style }
				onRequestClose={ this.requestClose.bind( this ) }
				anchorEl={ this.state.status_popover_anchor }
				targetOrigin={
					{
						horizontal: 'left',
						vertical: 'top'
					}
				}
				anchorOrigin={
					{
						horizontal: 'left',
						vertical: 'top'
					}
				}
				open={ this.state.status_popover_show }>
				{ items }
				</Popover>
			</div>
		);
	}
}
