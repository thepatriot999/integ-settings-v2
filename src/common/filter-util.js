export const FILTER_TYPES = {
	'integration_class' : [
		'All',
		'Integration',
		'Automation'
	],
	'integration_types' : [
		'All Integrations',
		'full',
		'full_account',
		'incremental'
	],
	'status_types' : [
		'Any Status',
		'automated_error',
		'pending_transmit',
		'pending_update',
		'pending_delete',
		'pending_confirm',
		'peding_transmit_waiting',
		'pending_automated_confirm',
		'pending_automated_delete'
	]
};

export function applyFilter ( filter, jobs ) {
	if ( !filter ) {
		return jobs;
	}

	if ( filter.integration_type_filter.length ) {
		jobs = jobs.filter( ( job ) => {
			return filter.integration_type_filter.indexOf( job.integration_type ) !== -1;
		} );
	}

	if ( filter.integration_class_filter !== 0 ) {
		if ( filter.integration_class_filter === 1 ) {
			jobs = jobs.filter( ( job ) => {
				return job.transmitter !== 38;
			} );
		} else if ( filter.integration_class_filter === 2 ) {
			jobs = jobs.filter( ( job ) => {
				return job.transmitter === 38;
			} );
		}
	}

	if ( filter.status_filter.length ) {
		jobs = jobs.filter( ( job ) => {
			return filter.status_filter.indexOf( job.job_status ) !== -1;
		} );
	}

	return jobs;
}

export function formatAndCleanRawList ( raw ) {
	let list = [];

	if ( raw.trim() !== '' ) {
		list = raw.split('\n');

		list = list.map( ( item ) => {
			return item.trim();
		} ).filter( ( item ) => {
			if ( item === '' ) {
				return false;
			}

			if ( /[^0-9]/.test( item ) ) {
				return false;
			}

			return true;
		} );
	}

	return list;
}

export function normalizePostingList ( postingList ) {
	let postings = {};

	postingList.forEach( ( posting ) => {
		postings[ posting.posting_id ] = posting;
	} );

	return postings;
}
