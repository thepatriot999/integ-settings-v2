import webpack from 'webpack';
import config from '../config/compile';

let compiler = webpack( config.webpack );

compiler.run( function ( err, stats ) {
	if ( err ) {
		console.log( 'Webpack failed: ' + err );
	}

	if ( stats.compilation.warnings.length ) {
		console.log( 'Warnings:' );
		console.log( stats.compilation.warnings );
	}

	if ( stats.compilation.errors.length ) {
		console.log( 'Errors:' );
		console.log( stats.compilation.errors );
	}
} );
